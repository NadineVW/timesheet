**Employee Dashboard**
The time sheet is used to track time for employees. The dashboard can be extended to log leave | claims etc for the employee

**How to deploy**

1) Download the .zip folder and add it to your SharePoint Site Assets folder.
2) Download the list template and import it to your SharePoint site. Create a new list called "TimeSheet" and use this template.
3) Add a link that points to the data.aspx file for easy access.
